removeButton.onclick = function() {
    var div = document.getElementById("skills");
    div.removeChild(div.lastChild);
    div.removeChild(div.lastChild);
    var counter = document.getElementById("skillsCounter");
    var counterValue = +counter.innerHTML;
    counter.innerHTML = --counterValue;
}

addButton.onclick = function() {
    var input = document.createElement("input");
    var br = document.createElement("br");
    input.setAttribute("type", "text");
    input.setAttribute("name", "skill");
    var div = document.getElementById("skills");
    div.appendChild(input);
    div.appendChild(br);
    var counter = document.getElementById("skillsCounter");
    var counterValue = +counter.innerHTML;
    counter.innerHTML = ++counterValue;
};

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

skillsHeader.onmousemove = function() {
    var header = document.getElementById("skillsHeader");
    header.style.color = getRandomColor();
}