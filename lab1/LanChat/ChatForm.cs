﻿using System;

using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace LanChat
{
    public partial class ChatForm : Form
    {
        private static readonly int port = 5010;

        private static void Send(string datagram)
        {
            UdpClient sender = new UdpClient();

            IPEndPoint endPoint = new IPEndPoint(IPAddress.Broadcast, port);

            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(datagram);

                sender.Send(bytes, bytes.Length, endPoint);
            }
            finally
            {
                sender.Close();
            }
        }

        public ChatForm()
        {
            InitializeComponent();
            var messageReceiver = new MessageReceiver(chatBox, port);
            Thread receiveThread = new Thread(messageReceiver.Receive);
            receiveThread.IsBackground = true;
            receiveThread.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Send($"{nameBox.Text}: {messageBox.Text}");
        }
    }
}
