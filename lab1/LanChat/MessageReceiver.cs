﻿using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace LanChat
{
    class MessageReceiver
    {
        private readonly ListBox _container;
        private readonly int _port;
        private UdpClient _receivingUdpClient;

        public MessageReceiver(ListBox container, int port)
        {
            _container = container;
            _port = port;
        }

        public void Receive()
        {
            IPEndPoint remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

            _receivingUdpClient = new UdpClient(_port);

            while (true)
            {
                byte[] receiveBytes = _receivingUdpClient.Receive(
                   ref remoteIpEndPoint);

                string returnData = Encoding.UTF8.GetString(receiveBytes);
                _container.Items.Add(returnData);
            }
        }
    }
}
