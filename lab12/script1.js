removeButton.onclick = function() {
    $("input[name='skill']:last").remove();
    $("br:last").remove();
    $("#skillsCounter").html($("input[name='skill']").length);
}

addButton.onclick = function() {
    $("#skills").append("<input type='text' name='skill'></input>");
    $("#skills").append("<br>");
    $("#skillsCounter").html($("input[name='skill']").length);
};

function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

skillsHeader.onmousemove = function() {
    $("#skillsHeader").css("color", getRandomColor());
}