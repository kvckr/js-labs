updateButton.onclick = function() {
    getWeather();
};

function getWeather() {
    $.get("http://api.openweathermap.org/data/2.5/weather", { "id": "625144", "units": "metric", "appid": "63ad7b9c6deaae8ddc773890776f7c58" })
        .done(function(data) {
            $("#temperature").html(data.main.temp);
            $("#humidity").html(data.main.humidity);
            $("#weatherSpeed").html(data.wind.speed);
            $("#weatherDirection").html(data.wind.deg);
        });
}