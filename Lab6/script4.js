function toInt(value) {
    var intVal = +value
    if (isNaN(intVal)) {
        throw new Error("Неправильный формат данных: " + value)
    }
    return intVal;
}

var weekday = new Array(7);
weekday[0] = "Воскресенье";
weekday[1] = "Понедельник";
weekday[2] = "Вторник";
weekday[3] = "Среда";
weekday[4] = "Четверг";
weekday[5] = "Пятница";
weekday[6] = "Суббота";


try {
    month = toInt(prompt("Введите номер месяца")) - 1;
    number = toInt(prompt("Введите число"));

    date = new Date(2015, month, number);

    var n = weekday[date.getDay()];

    alert(n);
}
catch (e) {
    alert(e.message);
}