function fib(n) {
    var a = 1,
        b = 1;
    for (var i = 3; i <= n; i++) {
        var c = a + b;
        a = b;
        b = c;
    }
    return b;
}

function toInt(value) {
    var intVal = +value
    if (isNaN(intVal)) {
        throw new Error("Неправильный формат данных: " + value)
    }
    return intVal;
}

try {
    i = toInt(prompt("Введите i"));
    if (i <= 0) {
        throw new Error("Неправильное число");
    }
    alert(fib(i));
}
catch (e) {
    alert(e.message);
}