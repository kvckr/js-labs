function toInt(value) {
    var intVal = +value
    if (isNaN(intVal)) {
        throw new Error("Неправильный формат данных: " + value)
    }
    return intVal;
}

try {
    floorsCount = toInt(prompt("Введите число этажей"));
    entrancesCount = toInt(prompt("Введите число подъездов"));
    floorApartmensCount = toInt(prompt("Введите количество квартир на этаже"));
    numberOfApartment = toInt(prompt("Введите номер квартиры"));

    apartmensInEntrance = floorsCount * floorApartmensCount;
    numberOfApartmens = apartmensInEntrance * entrancesCount;
    if (numberOfApartment > numberOfApartmens || numberOfApartment <= 0) {
        throw new Error("Неправильный номер квартиры");
    }
    numberOfEntrance = (numberOfApartment - 1) / apartmensInEntrance + 1;
    alert(Math.floor(numberOfEntrance));
}
catch (e) {
    alert(e.message);
}