function Vector(x, y, z) {
    this.x = x;
    this.y = y;
    this.z = z;
}

Vector.prototype.plus = function(other) {
    return new Vector(this.x + other.x, this.y + other.y, this.z + other.z);
}

Vector.prototype.scalar = function(other) {
    return (this.x * other.x + this.y * other.y + this.z * other.z);
}


Vector.prototype.valueOf = function() {
    var arr = [];
    arr.push(this.x);
    arr.push(this.y);
    arr.push(this.z);
    return arr;
}

Vector.prototype.toString = function() {
    var str = "" + this.x + "," + this.y + "," + this.z;
    return str;
}


Object.defineProperty(Vector.prototype, "length", {
    get: function myProperty() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }
});

var v1 = new Vector(1, 2, 3);
v1.x = 10;
var v2 = new Vector(3, 2, 1);
var v3 = v1.plus(v2);

alert(v3);
alert(v3.scalar(v1));