function createMatrix(rowsCount, columnsCount) {
    var matrix = [];
    for (var i = 0; i < rowsCount; i++) {
        matrix[i] = new Array(columnsCount);
        for (var j = 0; j < columnsCount; j++) {
            matrix[i][j] = Math.round(Math.random() * 100);
        }
    }
    return matrix;
}

function matrixToString(matrix) {
    var str = "";
    for (var i = 0; i < matrix.length; i++) {
        for (var j = 0; j < matrix[i].length; j++) {
            str += matrix[i][j] + ",";
        }
        str += "\n";
    }
    return str;
}

function checkMatrices(m1, m2) {
    return m1.length === m2.length && m1[0].length === m2[0].length;
}

function matrixSum(m1, m2) {
    var newMatrix = [];
    if (!checkMatrices(m1, m2)) {
        alert("Матрицы разной размерности");
        return;
    } else {
        for (var i = 0; i < m1.length; i++) {
            newMatrix[i] = new Array(m1[i].length);
            for (var j = 0; j < m1[i].length; j++) {
                newMatrix[i][j] = m1[i][j] + m2[i][j];
            }
        }
    }
    return newMatrix;
}

var n1 = +prompt("Введите размерность 1-й матрицы");
var n2 = +prompt("Введите размерность 2-й матрицы");
var m1 = createMatrix(n1, n1);
var m2 = createMatrix(n2, n2);
alert(matrixToString(m1));
alert(matrixToString(m2));
var m3 = matrixSum(m1, m2);
alert(matrixToString(m3));