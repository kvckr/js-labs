function isRectangle(x1, y1, x2, y2, x3, y3, x4, y4) {
    var firstDiagonal = Math.sqrt(Math.pow(Math.abs(x1 - x2), 2) + Math.pow(Math.abs(y2 - y3), 2));
    var secondDiagonal = Math.sqrt(Math.pow(Math.abs(x1 - x2), 2) + Math.pow(Math.abs(y1 - y4), 2));
    return firstDiagonal === secondDiagonal;
}

function isInRectangle(x1, y1, x2, y2, x3, y3, x4, y4, x5, y5) {
    var xMax = Math.max(x1, x2, x3, x4);
    var xMin = Math.min(x1, x2, x3, x4);
    var yMax = Math.max(y1, y2, y3, y4);
    var yMin = Math.min(y1, y2, y3, y4);
    var rightX = x >= xMin && x <= xMax;
    var rightY = y >= yMin && y <= yMax;
    return (rightX && rightY);
}

var x1 = prompt("Введите x1");
var y1 = prompt("Введите y1");
var x2 = prompt("Введите x2");
var y2 = prompt("Введите y2");
var x3 = prompt("Введите x3");
var y3 = prompt("Введите y3");
var x4 = prompt("Введите x4");
var y4 = prompt("Введите y4");
var x = prompt("Введите x");
var y = prompt("Введите y");

var isRect = isRectangle(x1, y1, x2, y2, x3, y3, x4, y4);
alert(isRect ?
    "Точки образуют прямоугольник" :
    "Точки не образуют прямоугольник");
if (isRect) {
    alert(isInRectangle(x1, y1, x2, y2, x3, y3, x4, y4, x, y) ?
        "Точка принадлежит прямоугольнику" :
        "Точка не принадлежит прямоугольнику");
}