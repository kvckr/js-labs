function reverseNumber(arg) {
    return -arg;
}

function numberCheck(func) {
    function wrapper(arg) {
        if (typeof arg !== "number") {
            alert("Аргумент не число");
        } else {
            return func.call(this, arg);
        }
    }
    return wrapper;
}

reverseNumber = numberCheck(reverseNumber);

alert(reverseNumber(2));
alert(reverseNumber("123"));


function countArguments() {
    return arguments.length;
}

function typeCheck(func, type) {
    function wrapper() {
        var args = Array.prototype.slice.call(arguments);
        for (var i = args.length - 1; i >= 0; i--) {
            if (typeof args[i] !== type) {
                alert("Аргумент не принадлежит типу " + type);
                return;
            }
        }
        return func.apply(this, arguments);
    }
    return wrapper;
}

countArguments = typeCheck(countArguments, "number");

alert(countArguments(1, 2, 3));
alert(countArguments(1, 2, 3, "123"));