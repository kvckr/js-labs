class Vector { // ES2015 Class Definition
    constructor(x, y, z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    plus(other) {
        return new Vector(this.x + other.x, this.y + other.y, this.z + other.z);
    }

    scalar(other) {
        return (this.x * other.x + this.y * other.y + this.z * other.z);
    }

    valueOf() {
        var arr = [];
        arr.push(this.x);
        arr.push(this.y);
        arr.push(this.z);
        return arr;
    }

    toString() {
        var str = `${this.x},${this.y},${this.z}`;
        return str;
    }

    get length() { // ES2015 Getter
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }
}

var v1 = new Vector(1, 2, 3);
v1.x = 10;
var v2 = new Vector(3, 2, 1);
var v3 = v1.plus(v2);

alert(v3);
alert(v3.scalar(v1));