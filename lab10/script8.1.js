function range(start, end, step = 1) { // ES2015 Default Parameter Values
    var arr = [];
    var current = 0;
    if (start > end && step > 0) {
        return [];
    }
    current = start;
    while (true) {
        arr.push(current);
        current += step;
        if (step > 0) {
            if (current > end) {
                break;
            }
        } else {
            if (current < end) {
                break;
            }
        }
    }
    return arr;
}

var start = +prompt("Введите начальное число");
var end = +prompt("Введите конечное число");
var step = +prompt("Введите шаг (необязательно)");

alert(range(start, end, step));