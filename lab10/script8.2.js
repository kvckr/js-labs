function bubbleSort(arr, comparator) {
    var done = false;
    while (!done) {
        done = true;
        for (var i = 1; i < arr.length; i++) {
            if (comparator(arr[i - 1], arr[i])) {
                done = false;
                let tmp = arr[i - 1]; // ES2015 Block-Scoped Variables
                arr[i - 1] = arr[i];
                arr[i] = tmp;
            }
        }
    }
    return arr;
}

function normalCompare(num1, num2) {
    return num1 > num2;
}

function absoluteCompare(num1, num2) {
    return Math.abs(num1) > Math.abs(num2);
}

var numberStrings = prompt("Введите массив чисел через запятую").split(",");
var arr = [];
for (var i = numberStrings.length - 1; i >= 0; i--) {
    arr.push(+numberStrings[i]);
}

alert(bubbleSort(arr, normalCompare));
alert(bubbleSort(arr, absoluteCompare));