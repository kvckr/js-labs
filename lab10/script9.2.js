class Task {
    constructor(name, description, startDate, endDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.subTasks = [];
    }
}

class PerformedTask extends Task { //ES2015 Class Inheritance
    constructor(name, description, startDate, endDate)
    {
        super(name, description, startDate, endDate);
        this.percent = 0;
        this.isFinished = false;
    }
}

PerformedTask.prototype = Object.create(Task.prototype);

var t1 = new Task("Задание", "Описание", new Date(2016, 10, 10), new Date(2016, 10, 11));
alert(t1 instanceof Task);
var t2 = new PerformedTask("Задание", "Описание", new Date(2016, 10, 10), new Date(2016, 10, 11));
alert(t2 instanceof Task);
alert(t2 instanceof PerformedTask);
alert(t2.name);