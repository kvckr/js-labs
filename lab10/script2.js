class BinaryHeap {
    constructor(scoreFunction) {
        this.content = [];
        this.scoreFunction = scoreFunction;
    }

    push(element) {
        this.content.push(element);
        this.bubbleUp(this.content.length - 1);
    }

    pop() {
        const result = this.content[0];
        const end = this.content.pop();
        if (this.content.length > 0) {
            this.content[0] = end;
            this.sinkDown(0);
        }
        return result;
    }

    remove(node) {
        const length = this.content.length;
        for (let i = 0; i < length; i++) {
            if (this.content[i] != node) continue;
            const end = this.content.pop();
            if (i == length - 1) break;
            this.content[i] = end;
            this.bubbleUp(i);
            this.sinkDown(i);
            break;
        }
    }

    size() {
        return this.content.length;
    }

    bubbleUp(n) {
        const element = this.content[n], score = this.scoreFunction(element);
        while (n > 0) {
            const parentN = Math.floor((n + 1) / 2) - 1, parent = this.content[parentN];
            if (score >= this.scoreFunction(parent))
                break;

            this.content[parentN] = element;
            this.content[n] = parent;
            n = parentN;
        }
    }

    sinkDown(n) {
        const length = this.content.length, element = this.content[n], elemScore = this.scoreFunction(element);

        while (true) {
            const child2N = (n + 1) * 2, child1N = child2N - 1;
            let swap = null;
            if (child1N < length) {
                const child1 = this.content[child1N];
                var child1Score = this.scoreFunction(child1);
                if (child1Score < elemScore)
                    swap = child1N;
            }
            if (child2N < length) {
                const child2 = this.content[child2N], child2Score = this.scoreFunction(child2);
                if (child2Score < (swap == null ? elemScore : child1Score))
                    swap = child2N;
            }

            if (swap == null) break;

            this.content[n] = this.content[swap];
            this.content[swap] = element;
            n = swap;
        }
    }
}

class HuffmanEncoding {
    constructor(str) {
        this.str = str;

        const count_chars = {};
        for (var i = 0; i < str.length; i++)
            if (str[i] in count_chars)
                count_chars[str[i]]++;
            else
                count_chars[str[i]] = 1;

        const pq = new BinaryHeap(x => x[0]);
        for (let ch in count_chars)
            pq.push([count_chars[ch], ch]);

        while (pq.size() > 1) {
            const pair1 = pq.pop();
            const pair2 = pq.pop();
            pq.push([pair1[0] + pair2[0], [pair1[1], pair2[1]]]);
        }

        const tree = pq.pop();
        this.encoding = {};
        this._generate_encoding(tree[1], "");

        this.encoded_string = ""
        for (var i = 0; i < this.str.length; i++) {
            this.encoded_string += this.encoding[str[i]];
        }
    }

    _generate_encoding(ary, prefix) {
        if (ary instanceof Array) {
            this._generate_encoding(ary[0], `${prefix}0`);
            this._generate_encoding(ary[1], `${prefix}1`);
        }
        else {
            this.encoding[ary] = prefix;
        }
    }

    decode(encoded) {
        const rev_enc = {};
        for (let ch in this.encoding)
            rev_enc[this.encoding[ch]] = ch;

        let decoded = "";
        let pos = 0;
        while (pos < encoded.length) {
            let key = "";
            while (!(key in rev_enc)) {
                key += encoded[pos];
                pos++;
            }
            decoded += rev_enc[key];
        }
        return decoded;
    }
}

var s = "testtest";
alert(s);

var huff = new HuffmanEncoding(s);

var es = huff.encoded_string;
alert(es);

var ds = huff.decode(es);
alert(ds);

alert(s === ds);
